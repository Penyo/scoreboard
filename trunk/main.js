﻿document.addEventListener("DOMContentLoaded", function documentReady() {
    main.init();
});

var main = (function () {

    function _addNewParticipant() {
        var txtNewPart = document.getElementById("txtNewParticipantName");
        if (txtNewPart != null) {
            var partName = txtNewPart.value,
                addParticipant = true;
            if (partName == "") {
                alert("Please, enter a name.");
                return;
            }
            if (ScoreBoard.participantExist(partName)) {
                addParticipant = confirm("Participant with entered name exist. Do you want to override it?");
            }
            if (addParticipant) {
                ScoreBoard.addParticipant(partName);
                txtNewPart.value = "";
                var participant = ScoreBoard.getParticipant(partName),
                    tableSbBody = _getScoreBoardTableBody(),
                    html = _getRowHTML(participant);
                var tr = document.createElement("tr");
                tr.innerHTML = html;
                tableSbBody.appendChild(tr);
                _addEventsToRowControls(tr);
                _setFooterText();
            }
        }
    };

    function _addEventsToRowControls(tr) {
        var btnAddPoints = tr.getElementsByClassName("btnAddPartPoints");
        btnAddPoints[0].onclick = _addPoints;
        var btnDeleteParticipant = tr.getElementsByClassName("btnDeleteParticipant");
        btnDeleteParticipant[0].onclick = _removeParticipant;
    }

    function _removeParticipant(event) {
        var btn = event.target,
            row = _getClosestRow(btn),
            tdName = row.getElementsByClassName("cell-name")[0],
            name = tdName.innerHTML;
        ScoreBoard.removeParticipant(name);
        row.remove();
        _setFooterText();
    }

    function _addPoints(event) {
        var btn = event.target,
            row = _getClosestRow(btn),
            txtPartPoints = row.getElementsByClassName("txt-add-participant-points")[0],
            scores = txtPartPoints.value,
            tdName = row.getElementsByClassName("cell-name")[0],
            name = tdName.innerHTML;
        if (isNaN(scores)) {
            alert("Entered scores is not a valid number.");
            return;
        }
        ScoreBoard.givePoints(name, parseInt(scores));
        var participantData = ScoreBoard.getParticipant(name);
        row.getElementsByClassName("cell-scores")[0].innerHTML = participantData.scores;
        row.getElementsByClassName("cell-beers")[0].innerHTML = participantData.beers;
        txtPartPoints.value = 0;
        _setFooterText();
    }

    function _getClosestRow(el) {
        var parent = el.parentNode;
        while (parent != null && parent.tagName.toLowerCase() != "tr") {
            parent = parent.parentNode;
        }
        return parent;
    }

    //function _parseParticipantsDataInTemplate() {
    //    var tableSbBody = _getScoreBoardTableBody(),
    //        data = ScoreBoard.listParticipants
    //    for (var name in data) {
    //        var participantData = data[name];
    //        var rowHTML = _getRowHTML(participantData);
    //        tableSbBody.innerHTML += rowHTML;
    //    }
    //}

    function _getScoreBoardTableBody() {
        var tableSbBody = document.querySelector("#tblScoreboard tbody");
        if (tableSbBody == null) {
            throw "Can't get ScoreBoard table element.";
        }
        return tableSbBody;
    }

    function _getRowHTML(participantData) {
        var templateEl = document.getElementById("tmplTableRow");
        if (templateEl == null) {
            throw "Can't find the template.";
        }
        var template = templateEl.innerHTML;
        var patternsToReplace = template.match(/({{.*?}})/g);
        var rowHTML = template;
        for (var i = 0; i < patternsToReplace.length; i++) {
            var pattern = patternsToReplace[i],
                key = pattern.replace(/{/g, "").replace(/}/g, "");
            rowHTML = rowHTML.replace(pattern, participantData[key] || 0);
        }
        return rowHTML;
    }

    function _setFooterText() {
        var footerCell = document.getElementById("tdFooterInfo"),
            participantsLength = Object.keys(ScoreBoard.listParticipants()).length,
            footerHTML = "No participants in the ScoreBoard.";
        if (participantsLength > 0) {
            footerHTML = "Total beers given: <b>" + ScoreBoard.beersGiven() + "</b>";
        }
        footerCell.innerHTML = footerHTML;
    }

    function init() {
        var btnNewPart = document.getElementById("btnAddNewParticipant");
        if (btnNewPart != null) {
            btnNewPart.onclick = _addNewParticipant;
        }

        //_parseParticipantsDataInTemplate();

        //btnsArray = document.querySelectorAll("#tblScoreboard tbody button.btnDeleteParticipant");
        //for (var i = 0 ; i < btnsArray.length; i++) {
        //    btnsArray[i].onclick = _removeParticipant;
        //}
    }

    var publicAPI = {
        init: init
    };
    return publicAPI;
})();